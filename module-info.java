/**
 * 
 */
/**
 * @author Michelle Rausch
 *
 */
module VoiceTrainingWithResonance {
	exports com.mooo.mrausch.resonance.middletier.users;
	exports com.mooo.mrausch.resonance.backend.users.tags;
	exports com.mooo.mrausch.resonance.frontend;
	exports tests.DAO;
	exports com.mooo.mrausch.resonance.middletier.analyze;
	exports com.mooo.mrausch.resonance.middletier.audio;
	exports tests.gui;
	exports com.mooo.mrausch.resonance.backend.audio;
	exports com.mooo.mrausch.resonance.backend.text;
	exports com.mooo.mrausch.resonance.backend.db;
	exports tests.audio;
	exports com.mooo.mrausch.resonance.backend.statistics;
	exports tests;
	exports com.mooo.mrausch.resonance.frontend.controllers;
	exports com.mooo.mrausch.resonance.middletier;
	exports tests.analyze;
	exports tests.db;
	exports com.mooo.mrausch.resonance.backend.users;
	exports com.mooo.mrausch.resonance.middletier.text;

	requires TarsosDSP;
	requires java.desktop;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	
	opens tests.gui;
	opens com.mooo.mrausch.resonance.frontend;
	opens com.mooo.mrausch.resonance.frontend.controllers;
}