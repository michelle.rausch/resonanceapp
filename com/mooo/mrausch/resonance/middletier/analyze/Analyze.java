/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier.analyze;



/**
 * This class analyzes the Audiostream with TarsosDSP
 * @author Michelle

 */

public class Analyze {
	/*
	 * Attributes
	 */

	/**
	 * The pitch of the audio during recording
	 */
	private double currentPitch;
	/**
	 * resonance of the audio during recording
	 */
	private double currentResonance;
	/**
	 * Average pitch over the audio-sample
	 */
	private double pitch;
	/**
	 * Minimum pitch over audio-sample 
	 */
	private double minPitch;
	/**
	 * Maximum pitch over audio-sample
	 */
	private double maxPitch;
	/**
	 * Average resonance over the whole audio-sample 
	 */
	private double resonance;
	
	/**
	 * Minimum Resonance over audio-sample
	 */
	private double minResonance;
	/**
	 * Maximum Resonance over audio-sample
	 */
	private double maxResonance;


	//analyze
	//TODO: FFT with JTransform
	
	//TODO: find peaks
	
	
	//TODO: implement/invent formula to get resonance from peaks
	//https://www.youtube.com/watch?v=-D9UlPcJSRM
	//http://faculty.tamuc.edu/cbertulani/music/lectures/Lec14/Lec14.pdf
	
	//TODO: run formula to get resonance,...
	
	
	
	//TODO: output: pitch, Resonance, in future: brassiness, breathiness
	
	//TODO: readable output, getters
	
	
	/*
	 * Getters & Setters
	 */
	/**
	 * current pitch during recording. To be displayed in GUI
	 * @return
	 */
	public double getCurrentPitch() {
		return currentPitch;
	}
	
	/**
	 * current resonance during recording. To be displayed in GUI
	 * @return
	 */
	public double getCurrentResonance() {
		return currentResonance;
	}
	
	/**
	 * average pitch over recording, for statistics
	 * @return
	 */
	public double getPitch() {
		return pitch;
	}
	/**
	 * average resonance over recording, for statistics
	 * @return
	 */	
	public double getResonance() {
		return resonance;
	}

	/**
	 * @return the minPitch
	 */
	public double getMinPitch() {
		return minPitch;
	}

	/**
	 * @param minPitch the minPitch to set
	 */
	public void setMinPitch(double minPitch) {
		this.minPitch = minPitch;
	}

	/**
	 * @return the maxPitch
	 */
	public double getMaxPitch() {
		return maxPitch;
	}

	/**
	 * @param maxPitch the maxPitch to set
	 */
	public void setMaxPitch(double maxPitch) {
		this.maxPitch = maxPitch;
	}

	/**
	 * @return the minResonance
	 */
	public double getMinResonance() {
		return minResonance;
	}

	/**
	 * @param minResonance the minResonance to set
	 */
	public void setMinResonance(double minResonance) {
		this.minResonance = minResonance;
	}

	/**
	 * @return the maxResonance
	 */
	public double getMaxResonance() {
		return maxResonance;
	}

	/**
	 * @param maxResonance the maxResonance to set
	 */
	public void setMaxResonance(double maxResonance) {
		this.maxResonance = maxResonance;
	}
	
	

}
