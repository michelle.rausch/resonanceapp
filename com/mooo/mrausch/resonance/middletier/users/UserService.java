/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier.users;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.mooo.mrausch.resonance.backend.users.User;
import com.mooo.mrausch.resonance.backend.users.UserOnMic;
import com.mooo.mrausch.resonance.backend.users.UsersDAO;

/**
 * This class handles operations on the Users implemented by DAO
 * 
 * These include 
 * 		CRUD
 * 		creating a "proper" user out of "UserOnMic"
 * 		save user Locally and to database
 * 
 * @author Michelle Rausch
 *
 */
public class UserService {
	
	/*
	 * Attributes
	 */
	/**
	 * The DAO for accessing all Users
	 */
	private UsersDAO source;		
	/**
	 * The List of all Users from online-Database
	 */
	private List<User> allUsers = new ArrayList<User>();
	
	/**
	 * The List of all locally saved Users
	 */
	private List<User> allLocalUsers = new ArrayList<User>();
	
	/*
	 * Constructors
	 */
	/**
	 * constructing the service from source
	 * @param source
	 */
	public UserService(UsersDAO userEntrySource) {
		this.source = userEntrySource;
		this.allUsers = userEntrySource.getAllUsers();
		UserOnMic.getInstance();
	}

	/*
	 * getter&setter
	 */
	/**
	 * @return the list of allUsers
	 */
	public List<User> getAllUsers() {
		return allUsers;
	}
	/**
	 * @return the list of all locally saved users
	 */
	public List<User> getAllLocalUsers() {
		return allLocalUsers;
	}

	/*
	 * Methods
	 */
	/**
	 * adds a user for the service using the defined implementation, 
	 * @param user
	 */
	public void addUser(User user) {
		source.addUser(user);
	}
	
	/**
	 * save the User locally
	 * @throws IOException 
	 */
	public void saveUserLocally(User user) throws IOException {
		File userFile = new File("res/users/"+ user.getUserID());
		ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(userFile));		
		objOut.writeObject(user);
	}
	
	/**
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 * 
	 */
	public void loadAllLocalUsersSeperate() {
		File folder = new File("res/users");
		File[] listOfFiles = folder.listFiles();
		for (File file : listOfFiles) {
		    if (file.isFile()) {
				ObjectInputStream objIn = null;
				try {
					objIn = new ObjectInputStream(new FileInputStream(file));
				} catch (IOException e) {
					System.err.println("file could not be loaded: " + file);
				}
				if (objIn != null) {
					try {
						allLocalUsers.add((User) objIn.readObject());
					} catch (ClassNotFoundException | IOException e) {
						System.err.println("file could not be converted to User: " + file);
					}
				}
		    } else {
		    	System.err.println("no valid filename, this should not happen, ever!");
		    }
		}	
	}
	
	/**
	 * saves all Local user into a single file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void saveAllLocalUsers() throws FileNotFoundException, IOException {
		File file = new File("res/users/userList");
		ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(file));	
		objOut.writeObject(allLocalUsers);
	}
	
	/**
	 * loads all local Users from single file
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" }) // taken care of
	public void loadAllLocalUsers() throws FileNotFoundException, IOException, ClassNotFoundException {
		File file = new File("res/users/userList");
		
		ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(file));
		Object readObject = objIn.readObject();
		if (readObject instanceof List) { //Checks, if it's a list
			List readList = (List) readObject;
			readList.forEach(listElement -> {
				if (listElement instanceof User) {
					allLocalUsers.add((User) listElement);
				}
			});
		}
				
	}
	
	
	
	
}
