/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier.users;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.mooo.mrausch.resonance.backend.users.UserOnMic;

/**
 * This class handles the File-system operations of the UserOnMic
 * 
 * @author Michelle Rausch
 *
 */
public class UserOnMicService {
	//Cannot extend Userservice, because too many unique properties of UserOn Mic: singleton, different save location, ...
	/**
	 * The file, where The UserOnMic is saved
	 */
	private File userOnMicFile = new File("res/settings/UserOnMic");
	
	/**
	 * constructs the service
	 */
	public UserOnMicService() {
		
	}
	
	public File getUserOnMicFile() {
		return userOnMicFile;
	}
	
	/**
	 * saves the UserOnMic
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void save() throws IOException {
		ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(userOnMicFile));		
		objOut.writeObject(UserOnMic.getInstance());
	}
	
	/**
	 * loads the UserOnMic, NEEDS to be executed on startup
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */	
	public void load(){
		
		if (userOnMicFile.isFile()) { //checks, if filepath makes sense
			ObjectInputStream objIn = null;
			try {
				objIn = new ObjectInputStream(new FileInputStream(userOnMicFile));
			} catch (IOException e) {
				System.out.println("could not load file " + userOnMicFile);
			}
			if (objIn != null) {

					try {
						UserOnMic.getInstance().setInstance((UserOnMic) objIn.readObject());
					} catch (ClassNotFoundException | IOException e) {
						System.out.println("could not convert file to UserOnMic");
					}
			}		
		} else {
			System.err.println("no valid filename");
		}
	}

}
