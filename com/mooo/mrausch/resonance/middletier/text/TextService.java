/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier.text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.text.TextDAO;
/**
 * This class handles operations on the Texts implemented by DAO
 * @author Michelle Rausch
 *
 */
public class TextService {
	
	/*
	 * Attributes
	 */
	/**
	 * The DAO for accessing all Texts
	 */
	TextDAO source;
	/**
	 * The list containing all Texts in database
	 */
	List<Text> allTexts = new ArrayList<Text>();
	
	/**
	 * contains all texts saved locally
	 */
	List<Text> allLocalTexts = new ArrayList<Text>();

	/*
	 * Constructors
	 */
	 /**
	  * constructing the service from source
	  * @param textEntrySource
	  */
	public TextService(TextDAO textEntrySource) {
		this.source = textEntrySource;
		this.allTexts = textEntrySource.getAllTexts();
	}
	
	/*
	 * getter & setter
	 */
	/**
	 * @return the list containing all Texts
	 */
	public List<Text> getAllTexts() {
		return allTexts;
	}
	
	/**
	 * @return all locally saved texts
	 */
	public List<Text> getAllLocalTexts() {
		return allLocalTexts;
	}
	
	
	
	/*
	 * Methods
	 */
	/**
	 * adds a text to the source
	 * @param text
	 */
	public void addText(Text text) {
		source.addText(text);
	}
	
	/**
	 * saves a text locally
	 * @param text The text to be saved
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void saveTextLocally(Text text) throws FileNotFoundException, IOException {
		File textFile = new File("res/texts/" + text.getTextID());
		ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(textFile));		
		objOut.writeObject(text);
		
	}
	
	/**
	 * loads all texts, that are stored locally
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	public void loadAllLocalTexts() {
		File folder = new File("res/texts");
		File[] listOfFiles = folder.listFiles();
		
		for (File file : listOfFiles) {
		    if (file.isFile()) {
				ObjectInputStream objIn = null;
				try {
					objIn = new ObjectInputStream(new FileInputStream(file));
				} catch (IOException e) {
					System.out.println("could not load file " + file);
				}
				if (objIn != null) {
			        try {
						allLocalTexts.add((Text) objIn.readObject());
					} catch (ClassNotFoundException | IOException e) {
						System.err.println("could not convert file " + file + " to Text");
					}
				}
		    } else {
		    	System.err.println(file + " is not a valid filename");
		    }
		}		
	}
	

	
}
