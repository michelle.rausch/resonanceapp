/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier.audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;


/**
 * Here the actual sound recording takes place
 * 		utilizes the sampled package
 * should also be singleton. One recording at a time
 * @author Michelle Rausch
 *
 */
public class Recording {
	
	/*
	 * Attributes
	 */
	private static Recording recording;
	/**
	 * The Dataline, that's being recorded
	 */
	private TargetDataLine line;
	
	/**
	 * The stream, where the line will be recorded to
	 */
	private AudioInputStream inputStream;
	
	/**
	 * if the recording is over
	 */
	private boolean recordingOver;
	
	/**
	 * the format used in the recording
	 */
	private AudioFormat format;
	
	private DataLine.Info info;
	
	/*
	 * Constructors
	 */
	/**
	 * contructs the recording
	 * @throws LineUnavailableException
	 */
	private Recording() {
		recordingOver = true;
		format = defineAudioFormat();
		info = new DataLine.Info(TargetDataLine.class, format); //defines the info for the DataLine
		if (!AudioSystem.isLineSupported(info)) {
			System.out.println("This AudioLine is not supported by the system");
		} else {
			try {
				line = (TargetDataLine) AudioSystem.getLine(info);
			} catch (LineUnavailableException e) {
				System.out.println("line is unavailable");
			}
		}
		
	}
	
	
	
	/*
	 * getter&setter
	 */
	/**
	 * returns the singleton
	 */
	public static Recording getInstance() {
		if (recording == null) {
			recording = new Recording();
		}
		return recording;
	}
	
	/**
	 * 
	 * @return the format used in the recording
	 */
	public AudioFormat getFormat() {
		return format;
	}
	
	/**
	 * @return whether the recording is over
	 */
	public boolean isRecordingOver() {
		return recordingOver;
	}
	
    /**
     * @return the AudioInputstream
     */
    public AudioInputStream getInputStream() {
		return inputStream;
	}

	
	/*
	 * Methods
	 */
	/**
	 * @throws LineUnavailableException
	 * Starts the recording till terminated
	 */
	public void startRecording() throws LineUnavailableException {

		recordingOver = false;
		
		line.open();

		line.start();
		
		inputStream = new AudioInputStream(line);
		
		System.out.println("started recording and capturing");
		
		
	}
	
	
	/**
	 * Starts the recording for specified time and ends it
	 * @param the time of recording
	 * @throws InterruptedException 
	 */
	public void recordForTime(long length) {

		
		//starts a new thread which sleeps for the specified time, then ends program
		final Thread stopper = new Thread(() ->{
			try {
				Thread.sleep(length);
			} catch (InterruptedException interrupt) {
				//do nothing on interrupt, just continue to end recording
			}
			
			endRecording();
		});	
		
		stopper.start();
		
		try {
			startRecording();
		} catch (LineUnavailableException e) {
			stopper.interrupt();
			e.printStackTrace();
		}
	}
	
	/**
	 * stops the recording
	 */
	public void endRecording() {
		if (line != null) {
			line.stop();
			line.close();
			
		}

		recordingOver = true;
		
		System.out.println("recording finished");
		
	}
	
	
	
	/**
	 * Defines and returns an audio-format
	 * @return the AudioFormat
	 */
    private AudioFormat defineAudioFormat() {
        float sampleRate = 16000;
        int sampleSizeInBits = 8;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = true;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                                             channels, signed, bigEndian);
        return format;
    } 
    

}
