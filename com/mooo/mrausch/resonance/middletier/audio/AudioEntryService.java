/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier.audio;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.mooo.mrausch.resonance.backend.audio.AudioEntry;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryDAO;
import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.users.User;
import com.mooo.mrausch.resonance.backend.users.UserOnMic;

/**
 * This class creates handles the Audioentries
 * 		It starts a recording
 * 		It issues an analyzation TODO
 * 		It creates an AudioEntry from the above
 * @author Michelle Rausch
 *
 */
public class AudioEntryService {
	
	/*
	 * Attributes
	 */
	/**
	 * The DAO for accessing AudioEntrys
	 */
	private AudioEntryDAO audioEntrySource;
	
	/**
	 * The List of all AudioEntrys
	 */
	private List<AudioEntry> allAudioEntrys = new ArrayList<AudioEntry>();
	

	
	/*
	 * constructors
	 */
	/**
	 * constructing the service from source
	 * @param audioEntrySource
	 */
	public AudioEntryService(AudioEntryDAO audioEntrySource) {
		this.audioEntrySource = audioEntrySource;
		this.allAudioEntrys = audioEntrySource.getAllAudioEntries();
	}
	/*
	 * getter & setter
	 */

	/**
	 * 
	 * @return The List of all AudioEntrys
	 */
	public List<AudioEntry> getAllAudioEntrys() {
		return allAudioEntrys;
	}
	
	/*
	 * Methods
	 */
	
	/**
	 * writes the given InputStream into the .wav file belonging to AudioEntry
	 * @param inputStream
	 * @throws IOException 
	 */
	public void writeRecordingIntoFile(AudioInputStream inputStream, AudioEntry audioEntry) throws IOException {

		AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;
		AudioSystem.write(inputStream, fileType, audioEntry.getAudioRecordingFile());	
		
	}
	
	/**
	 * records the audio for the entry, update Audioentry with new Audio
	 * @param audioEntry
	 */
	public void recordForAudioEntry(AudioEntry audioEntry) {

		Recording.getInstance();
		

		Recording.getInstance().recordForTime(audioEntry.getText().getRecordingTime());//starts recording for specified time

		//saves the recording as file
		try {
			this.writeRecordingIntoFile(Recording.getInstance().getInputStream(), audioEntry);
		} catch (IOException e) {
			System.err.println("could not save recording");
		}
	}
	
	/**
	 * plays the audioEntry via default-speakers currently with sampled, will TODO switch to JavaFX
	 * @param audioEntry the AudioEntry to be played back
	 */
	public void playAudioEntry(AudioEntry audioEntry) {
		try {
			File file = audioEntry.getAudioRecordingFile();
			System.out.println("start playback");
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(file);
			Clip clip = AudioSystem.getClip();
			
			clip.open(inputStream);
			clip.start();
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
			System.err.println("something has happened :("); //TODO actual error handling, or replace Method with JavaFX
		}

	}
	
	
	/**
	 * creates and Audioentry with the UserOnMic
	 * @param text
	 */
	public void createAudioEntry(Text text) {
		audioEntrySource.addAudioEntry(new AudioEntry(UserOnMic.getInstance(), text));
	}
	
	/**
	 * searches for all Audioentries with the Text text
	 * @param text the Text to filter by
	 * @return the list of all entries with the Text
	 */
	public List<AudioEntry> getAllAudioEntrysWithText(Text text) {
		List<AudioEntry> allAudioEntrysWithText = new ArrayList<AudioEntry>();
		allAudioEntrys.forEach(entry ->{
			if (entry.getText().equals(text)) {
				allAudioEntrysWithText.add(entry);
			}
		});
		return allAudioEntrysWithText;	
	}
	
	/**
	 * searches for all Audioentries with the User user
	 * @param text the Text to filter by
	 * @return the list of all entries with the Text
	 */
	public List<AudioEntry> getAllAudioEntrysOfUser(User user) {
		List<AudioEntry> allAudioEntrysOfUser = new ArrayList<AudioEntry>();
		allAudioEntrys.forEach(entry ->{
			if (entry.getUser().equals(user)) {
				allAudioEntrysOfUser.add(entry);
			}
		});
		return allAudioEntrysOfUser;	
	}
	
	/**
	 * searches for and returns a specific AudioEntry belonging to User with Text
	 * @param user
	 * @param text
	 * @return 
	 * @return specific AudioEntry, null if not found
	 */
	public AudioEntry getSpecificEntry(User user, Text text) {
		

		for (AudioEntry audioEntry : allAudioEntrys) {
			if (audioEntry.getText().equals(text) && audioEntry.getUser().equals(user)) {
				return audioEntry;
			}
		}
		
		return null;
	}
	
	

}
