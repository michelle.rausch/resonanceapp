/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier;

import java.util.List;

import com.mooo.mrausch.resonance.backend.audio.AudioEntry;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryDAO;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryDAOimpl;
import com.mooo.mrausch.resonance.backend.db.DatabaseConnection;
import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.text.TextDAO;
import com.mooo.mrausch.resonance.backend.text.TextDAOimpl;
import com.mooo.mrausch.resonance.backend.users.User;
import com.mooo.mrausch.resonance.backend.users.UserDAOimpl;
import com.mooo.mrausch.resonance.backend.users.UsersDAO;
import com.mooo.mrausch.resonance.middletier.audio.AudioEntryService;
import com.mooo.mrausch.resonance.middletier.audio.Recording;
import com.mooo.mrausch.resonance.middletier.text.TextService;
import com.mooo.mrausch.resonance.middletier.users.UserOnMicService;
import com.mooo.mrausch.resonance.middletier.users.UserService;

/**
 * This class contains all database-services, connects the frontend in one simple class to the entire backend
 * contains (references to)
 * 		the List of all texts,
 * 		the List of all reference-users
 * 		the List of all AudioEntrys
 * 		the services for the above
 * 
 * is a singleton: only one class of this exists in whole app, acessible from everywhere, especially frontend
 * @author Michelle Rausch
 *
 */
public class IOService {
	
	/*
	 * Attributes
	 */
	/**
	 * the IO-Service itself, part of singleton-pattern
	 */
	private static IOService ioService;
	
	/**
	 * The list of all Users, used for reference
	 */
	private List<User> allReferenceUsers;
	
	/**
	 * The List of all texts
	 */
	private List<Text> allTexts;
	
	/**
	 * The List of all AudioEntrys
	 */
	private List<AudioEntry> allAudioEntrys;
	
	/**
	 * The service handling text
	 */
	private TextService textService;
	
	/**
	 * The service handling Users
	 */
	private UserService userService;
	
	/**
	 * The service handling AudioEntrys
	 */
	private AudioEntryService audioEntryService;
	
	/**
	 * the database-connection
	 */
	private DatabaseConnection dbConnection;
	
	/**
	 * The Service for the active User
	 */
	private UserOnMicService userOnMicService;
	
	/**
	 * The audio-recording
	 */
	private Recording recording ;
	
	/*
	 * constructor
	 */
	/**
	 * contructs the IOService out of the different services
	 * @param textEntrySource
	 * @param userEntrySource
	 * @param audioEntrySource
	 */
	private IOService(TextDAO textEntrySource, UsersDAO userEntrySource, AudioEntryDAO audioEntrySource) {
		textService = new TextService(textEntrySource);
		allTexts = textService.getAllTexts();
		userService = new UserService(userEntrySource);
		allReferenceUsers = userService.getAllUsers();
		audioEntryService = new AudioEntryService(audioEntrySource);
		allAudioEntrys = audioEntryService.getAllAudioEntrys();
		
		dbConnection = new DatabaseConnection();
	}
	
	/**
	 * constructs the IOService out of the default DAO-implementations 
	 */
	private IOService() {
		initServices();
	}

	/*
	 * instance for singleton-pattern
	 */
	public static IOService getinstance() {
		if (ioService == null) {
			ioService = new IOService();
		}	
		return ioService;
	}
	
	/*
	 * Getter & Setter
	 */
	/**
	 * @return the dbConnection
	 */
	public DatabaseConnection getDbConnection() {
		return dbConnection;
	}

	/**
	 * @param dbConnection the dbConnection to set
	 */
	public void setDbConnection(DatabaseConnection dbConnection) {
		this.dbConnection = dbConnection;
	}

	/**
	 * @return the allReferenceUsers
	 */
	public List<User> getAllReferenceUsers() {
		return allReferenceUsers;
	}

	/**
	 * @return the allTexts
	 */
	public List<Text> getAllTexts() {
		return allTexts;
	}

	/**
	 * @return the allAudioEntrys
	 */
	public List<AudioEntry> getAllAudioEntrys() {
		return allAudioEntrys;
	}

	/**
	 * @return the textService
	 */
	public TextService getTextService() {
		return textService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * @return the userOnMicService
	 */
	public UserOnMicService getUserOnMicService() {
		return userOnMicService;
	}
	
	/**
	 * @return the audioEntryService
	 */
	public AudioEntryService getAudioEntryService() {
		return audioEntryService;
	}
	
	/*
	 * Methods
	 */
	/**
	 * initializes all services encapsules by this super-service
	 * loads local files
	 */
	private void initServices() {
		textService = new TextService(new TextDAOimpl());
		allTexts = textService.getAllTexts();
		userService = new UserService(new UserDAOimpl());
		allReferenceUsers = userService.getAllUsers();
		audioEntryService = new AudioEntryService(new AudioEntryDAOimpl());
		allAudioEntrys = audioEntryService.getAllAudioEntrys();
		userOnMicService = new UserOnMicService();
		dbConnection = new DatabaseConnection();	
		
		//loads local files
		userOnMicService.load();
		Settings.load();
		textService.loadAllLocalTexts();
		userService.loadAllLocalUsersSeperate();
		
	}
	
}
