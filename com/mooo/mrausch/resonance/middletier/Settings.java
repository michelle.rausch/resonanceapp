/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.middletier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.users.User;

/**
 * This class handles the settings, is singleton
 * @author Michelle Rausch
 *
 */
public class Settings  implements Serializable{
	
	/**
	 * serialID for saving/loading settings
	 */
	private static final long serialVersionUID = -8583990357865003290L;
	/*
	 * Attributes
	 */
	/**
	 * The instance itself
	 */
	private static Settings settings;
	/**
	 * Whether recording is active, when hitting paly
	 */
	private boolean recordingActive;

	/**
	 * Whether playback of reference is active, when hitting play
	 */
	private boolean playbackActive;

	/**
	 * Whether loopback (playback of own recording) is active, when hitting play
	 */
	private boolean loopbackActive;
	
	/**
	 * The currently selected User
	 */
	private User activeUser;
	
	/**
	 * The curently selected Text
	 */
	private Text activeText;

	/*
	 * Contructors
	 */
	/**
	 * constructs with everything turned off
	 * @param recording
	 * @param playback
	 * @param loopback
	 */
	private Settings() {
		recordingActive = false;
		playbackActive = false;
		loopbackActive = false;
		
		//TODO: load from file
	}
	
	/**
	 * @return the single Setings, that exist
	 */
	public static Settings getInstance() {
		if (settings == null) {
			settings = new Settings();
		}	
		return settings;
	}

	/*
	 * Setters & Getters
	 */
	/**
	 * @return the recording
	 */
	public boolean isRecordingActive() {
		return recordingActive;
	}

	/**
	 * @param recording the recording to set
	 */
	public void setRecordingActive(boolean recording) {
		this.recordingActive = recording;
	}

	/**
	 * @return the playback
	 */
	public boolean isPlaybackActive() {
		return playbackActive;
	}

	/**
	 * @param playback the playback to set
	 */
	public void setPlaybackActive(boolean playback) {
		this.playbackActive = playback;
	}

	/**
	 * @return the loopback
	 */
	public boolean isLoopbackActive() {
		return loopbackActive;
	}

	/**
	 * @param loopback the loopback to set
	 */
	public void setLoopbackActive(boolean loopback) {
		this.loopbackActive = loopback;
	}
	
	/**
	 * @return the activeUser
	 */
	public User getActiveUser() {
		return activeUser;
	}

	/**
	 * @param activeUser the activeUser to set
	 */
	public void setActiveUser(User activeUser) {
		this.activeUser = activeUser;
	}

	/**
	 * @return the activeText
	 */
	public Text getActiveText() {
		return activeText;
	}

	/**
	 * @param activeText the activeText to set
	 */
	public void setActiveText(Text activeText) {
		this.activeText = activeText;
	}

	/*
	 * Methods
	 */
	/**
	 * saves the settings
	 */
	public static void save() {
		File settingsFile = new File("res/settings/settings");
			
		try {
			ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(settingsFile));	
			objOut.writeObject(settingsFile);			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * loads the settings
	 */
	public static void load() {
		File settingsFile = new File("res/settings/settings");
	    if (settingsFile.isFile()) {
			ObjectInputStream objIn;
			try {
				objIn = new ObjectInputStream(new FileInputStream(settingsFile));
		        settings = ((Settings) objIn.readObject());
			} catch (IOException e) {
				System.err.println("could not read settings file");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				System.err.println("settings file corrupt");
				e.printStackTrace();
			}

	    }
	}
	
}
