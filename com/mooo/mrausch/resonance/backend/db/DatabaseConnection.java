/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.db;

/**
 * This class handles the connectionn to the database
 * Attributes: 
 * 		URL to localhost, until a proper server has been set up
 * 		USER='root' 
 * 		PASSWORD=''
 * 
 * @author Michelle Rausch
 *
 */
public class DatabaseConnection {
	/** The URL to the Server, for testing here localhost*/
	public static final String URL = "jdbc:mysql://localhost:3306/ResonanceTest?CreateIfNotExist?serverTimezone=UTC"; 
	/** The user for the database*/
	public  final String USER = "root";
	/** the password to the database*/
	public final String PASSWORT = "";
	
	
	
	

}
