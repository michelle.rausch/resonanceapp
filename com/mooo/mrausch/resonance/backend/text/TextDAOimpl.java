/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.text;

import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of the DAO for the texts, which shall be read alour
 * @author Michelle Rausch
 *
 */
public class TextDAOimpl implements TextDAO {

	/*
	 * Attributes
	 */
	/**
	 * The list of all Texts in Database
	 */
	private List<Text> allTexts;
	/**
	 * Holds the highest ID of al texts, necesary for incrementation
	 */
	private long highestID;
	
	/*
	 * constructors
	 */
	/**
	 * The constructor for the implementationi of the TextDAO
	 */
	public TextDAOimpl() {
		allTexts = new ArrayList<Text>();
	}
	
	/*
	 * help-methods
	 */
	/**
	 * finds and sets the highest ID, important for incrementation 
	 */
	private void findHighestID() {
		allTexts.forEach(text -> {
			if (highestID < text.getTextID()) {
				highestID = text.getTextID();
			}
		});
	}
	/*
	 * implemented Methods by TextDAO
	 */
	@Override
	public List<Text> getAllTexts() {
		return allTexts;
	}

	@Override
	public void addText(Text text) {
		findHighestID();
		text.setTextID(highestID + 1); 
		allTexts.add(text);
	}

	@Override
	public void deleteText(Text text) {
		allTexts.remove(text);
	}

	//TODO better solution
	@Override
	public void updateText(Text text) {
		deleteText(getText(text.getTextID()));
		addText(text);
		
	}
	
	/**
	 * returns the Text with textID
	 * @param textID the textID to seach for
	 * @return the first text with this ID
	 */
	public Text getText(long textID) {	
		for (Text text: allTexts) {
			if (text.getTextID() == textID) {
				return text;
			}
		}
	return null;	
	}
	
	

	
}
