/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.text;

import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;

/**
 * This class describes the texts that wil be read aloud
 * To be handled by database. 
 * Each text has an ID
 * Each text has a name
 * Each text has a locale
 * Each text has some stats, like 
 * 		length of recording
 * @author Michelle Rausch
 *
 */
public class Text implements Serializable{
	
	/*
	 * Attributes
	 */
	
	/**
	 * serial ID
	 */
	private static final long serialVersionUID = 4664472541738307429L;
	/**
	 * The ID of the Text, by Database
	 */
	protected long textID;

	/**
	 * The text to be read aloud
	 */
	protected String textToSay;
	/**
	 * Displayed as the name of the text, e.g poem
	 */
	protected String textName;
	
	/**
	 * The time it takes to read the text out loud
	 */
	protected int recordingTime;
	
	/**
	 * The locale of the text
	 */
	protected Locale locale;

	
	/*
	 * Contructors
	 */

	/**
	 * Constructor with all parameters, except recording time. 
	 * 			The recording will instead be halted manually
	 * @param textToSay The text to be read aloud
	 * @param textName The name of the text
	 * @param locale The locale of the text
	 */
	public Text(String textToSay, String textName, Locale locale) {
		this.textToSay = textToSay;
		this.textName = textName;
		this.locale = locale;
	}

	/**
	 * Constructor with all parameters
	 * @param textToSay The text to be read aloud
	 * @param textName The name of the text
	 * @param locale The locale of the text
	 */
	public Text(String textToSay, String textName, Locale locale, int recordingTime) {
		this.textToSay = textToSay;
		this.textName = textName;
		this.locale = locale;
		this.recordingTime = recordingTime;
	}

	/*
	 * Getter&Setter
	 */
	/**
	 * @return the textID
	 */
	public long getTextID() {
		return textID;
	}


	/**
	 * @param textID the textID to set
	 */
	public void setTextID(long textID) {
		this.textID = textID;
	}


	/**
	 * @return the textToSay
	 */
	public String getTextToSay() {
		return textToSay;
	}


	/**
	 * @param textToSay the textToSay to set
	 */
	public void setTextToSay(String textToSay) {
		this.textToSay = textToSay;
	}


	/**
	 * @return the textName
	 */
	public String getTextName() {
		return textName;
	}


	/**
	 * @param textName the textName to set
	 */
	public void setTextName(String textName) {
		this.textName = textName;
	}


	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * sets the recording time
	 * @param recordingTime
	 */
	public void setRecordingTime(int recordingTime) {
		this.recordingTime = recordingTime;
	}
	/**
	 * @return the time it takes to record this
	 */
	public int getRecordingTime() {
		return recordingTime;
	}
	
	/**
	 * Checks, whether the two ID's are the same
	 * @param text
	 * @return
	 */
	public boolean hasSameID(Text text) {
		return this.textID == text.getTextID();
	}

	/*
	 * Hashes equals and toString
	 */

	@Override
	public int hashCode() {
		return Objects.hash(locale, textName, textToSay);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Text)) {
			return false;
		}
		Text other = (Text) obj;
		return Objects.equals(locale, other.locale) && Objects.equals(textName, other.textName)
				&& Objects.equals(textToSay, other.textToSay);
	}


	@Override
	public String toString() {
		return "Text [textName=" + textName+ ", textToSay=" +  textToSay + ", locale=" + locale + "]";
	}
	
	
	

	

}
