/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.text;

import java.util.List;

/**
 * This interface describes the texts that will be read aloud
 * A text-entry can be added, deleted, updated 
 * 		
 * @author Michelle Rausch
 *
 */
public interface TextDAO {
	
	/**
	 * returns the list of all texts
	 * @return the list of all texts
	 */
	public List<Text> getAllTexts();
	
	/**
	 * Adds a text 
	 */
	public void addText(Text text);
	
	/**
	 * deletes a text
	 */
	public void deleteText(Text text);
	
	/**
	 * Updates a specific text
	 */
	public void updateText(Text text);

	

}
