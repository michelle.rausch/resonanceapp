/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.text;

import java.util.Locale;

/**
 * This sample contains some random Texts, which will be read out loud.
 * This is mostly for testing purposes as well as demonstrating, that I know how to user enums.
 * Will be used in case no Texts from database are used.
 * @author Michelle Rausch
 *
 */
public enum DefaultText {
	FIRE("Heat from fire - fire from heat"),
	WATER("Drink plenty of water!"),
	TRANS("Trans rights are human rights!"),
	JAVA("Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible."),
	LOREM("Cupiditate maxime sed porro quos a. Sunt labore sed aut esse eum necessitatibus atque.");

	/**
	 * The text to be read aloud
	 */
	private String textToSay;
	/**
	 * Displayed as the name of the text, e.g poem
	 */
	private String textName;
	
	/**
	 * The time it takes to read the text out loud
	 */
	private int recordingTime;
	
	/**
	 * The locale of the text
	 */
	private Locale locale;

	/**
	 * @param textToSay
	 * @param textName
	 * @param recordingTime
	 * @param locale
	 */
	private DefaultText(String textToSay) {
		this.textToSay = textToSay;
		this.textName = "Random Text";
		this.recordingTime = 5_000;
		this.locale = Locale.ENGLISH;
	}

	/**
	 * @return the textToSay
	 */
	public String getTextToSay() {
		return textToSay;
	}

	/**
	 * @return the textName
	 */
	public String getTextName() {
		return textName;
	}

	/**
	 * @return the recordingTime
	 */
	public int getRecordingTime() {
		return recordingTime;
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}
	
	public Text getTextObj() {
		return new Text(textToSay, textName, locale, recordingTime);
	}


}
