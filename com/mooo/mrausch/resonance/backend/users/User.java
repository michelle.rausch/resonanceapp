/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.users;

import java.io.Serializable;
import com.mooo.mrausch.resonance.backend.statistics.UserStatistics;
import com.mooo.mrausch.resonance.backend.users.tags.AGAB;
import com.mooo.mrausch.resonance.backend.users.tags.Gender;

/**
 * The class for the Users
 * is a Value-Object
 * A User has an ID (handled by Database)
 * A user has a name
 * A user has statistics -> own Database
 * 
 * @author Michelle Rausch
 *
 */
public class User implements Serializable {
	/**
	 * serial ID
	 */
	private static final long serialVersionUID = -7096906180074320368L;
	/*
	 * Attribute
	 */
	/**
	 *  The user-ID, handled by database
	 */
	protected long userID;
	/**
	 * The name of the user
	 */
	protected String name;
	
	/**
	 * The current gender of the user
	 */
	protected Gender gender;
	
	/**
	 * The AGAB (= assigned gender at birth) of the User 
	 */
	protected AGAB agab;
	
	/**
	 * The statistics of the user per TextToRead-entry
	 * and average over all
	 */
	protected UserStatistics stats;
	
	
	/*
	 * Konstruktoren
	 */
	
	/**
	 * Constructor with name only
	 * @param name
	 */
	public User(String name) {
		this.name = name;
	}
	
	
	
	/*
	 * getters&setters
	 */
	/**
	 * Returns the full Name of the user
	 * @return the full Name of the user
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the full name of the user
	 * @param name the full name of the user
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * returns the ID of the User
	 * @return the ID of the User
	 */
	public long getUserID() {
		return userID;
	}
	
	/**
	 * sets the userID
	 * @param userID
	 */
	public void setUserID(long userID) {
		this.userID = userID;
	}

	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * @return the agab
	 */
	public AGAB getAgab() {
		return agab;
	}

	/**
	 * @param agab the agab to set
	 */
	public void setAgab(AGAB agab) {
		this.agab = agab;
	}

	/**
	 * Checks, whether the two ID's are the same
	 * @param text
	 * @return
	 */
	public boolean hasSameID(User user) {
		return this.getUserID() == user.getUserID();
	}


	
	/*
	 * hash, equals, toString
	 */

	@Override
	public String toString() {
		return "User [name=" + name + ", gender=" + gender + ", agab=" + agab + "]";
	}
	
	
}
