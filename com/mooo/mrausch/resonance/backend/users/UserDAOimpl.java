/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.users;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements the UserDAO
 * @author Michelle Rausch
 *
 */
public class UserDAOimpl implements UsersDAO {

	/*
	 * attributes
	 */
	/**
	 * The list of all users
	 */
	private List<User> allUsers;
	
	/**
	 * holds the highestID of this set of Users, important for counting up
	 */
	private long highestID;
	
	
	/*
	 * Constructors
	 */
	/**
	 * The constructor for the implementation of the UserDAO
	 */
	public UserDAOimpl() {
		allUsers = new ArrayList<User>();
	}
	
	/*
	 * help-methods
	 */
	/**
	 * finds and sets the highest ID, important for incrementation 
	 */
	private void findHighestID() {
		allUsers.forEach(user -> {
			if (highestID < user.getUserID()) {
				highestID = user.getUserID();
			}
		});
	}
	
	/*
	 * methods by UserDAO
	 */
	
	@Override
	public List<User> getAllUsers() {
		return allUsers;
	}

	@Override
	public void addUser(User user) {
		findHighestID();
		user.setUserID(highestID + 1);
		allUsers.add(user);
		System.out.println("ID of added user:" + user.userID);
	}

	@Override
	public void deleteUser(User user) {
		allUsers.remove(user);	
	}

	//TODO better solution
	@Override
	public void updateUser(User user) {
		deleteUser(getUser(user.getUserID()));
		addUser(user);
	}
	
	/**
	 * returns the user with the specified ID, null if user does not exit
	 * @param userID the userID of the user that's searched for
	 * @return the user with the specified ID
	 */
	public User getUser(long userID) {
		for (User user : allUsers) {
			if (user.getUserID() == userID) {
				return user;
			}
		}
		return null;	
	}

}
