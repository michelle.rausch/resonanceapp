/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.users;

import java.util.List;

/**
 * Implements the User in a DAO (Data Object Interace) design pattern
 * A user can be added
 * A user can be deleted
 * A user can be updated
 * All users can be listed
 * 
 * @author Michelle Rausch
 *
 */
public interface UsersDAO {

	/**
	 * Lists all users
	 * @return a list of all users
	 */
	public List<User> getAllUsers();
	
	/**
	 * Adds a user 
	 */
	public void addUser(User user);
	
	/**
	 * deletes a user
	 */
	public void deleteUser(User user);
	
	/**
	 * Updates a specific User
	 */
	public void updateUser(User user);
	
	
}
