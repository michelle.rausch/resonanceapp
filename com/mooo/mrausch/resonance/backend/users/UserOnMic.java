/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.users;


import java.io.Serializable;


/**
 * 
 * The user currently using the programm, Only a single userOnMic exists, 
 * therefore using Singleton-Design-Pattern
 * This user is saved locally and is loaded from file-system
 * @author Michelle Rausch
 *
 */
public class UserOnMic extends User implements Serializable {
	
	/**
	 * The serialID for database
	 */
	private static final long serialVersionUID = 8117669479588824870L;
	
	
	/*
	 * Attributes
	 */
	
	private static UserOnMic userOnMic;
	
	/*
	 * Constructors
	 */
	/**
	 * constructor for the user currently using the app
	 */
	private UserOnMic() {
		super("UserOnMic");
	}
	

	/*
	 * Methods
	 */
	/**
	 * returns the instance of UserOnMic, or creates it, if not exist, part of Singleton design pattern
	 * @return the UserOnMic
	 */
	public static UserOnMic getInstance() {
		if (userOnMic == null) {
			userOnMic = new UserOnMic();		
		}
		return userOnMic;	
	}
	
	/**
	 * Turns the user into the UserOnMic
	 * @param user
	 * @return
	 */
	public void setInstance(User user) {
		this.agab = user.agab;
		this.name = user.name;
		this.gender = user.gender;
		this.stats = user.stats;
	}
	
	/**
	 * creates a user to be saved into the database from the current user
	 * This strips the UserOnMic from all unnecessary attributes and adds attributes for the database
	 * @return user to be saved into the database
	 */
	public User createUserForDatabase() {
		User newUser = new User(this.name);
		newUser.setAgab(this.agab);
		newUser.setGender(this.gender);
		newUser.stats = this.stats;
		return newUser;
	}
	
	
	

}
