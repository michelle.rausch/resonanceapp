/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.users.tags;

/**
 * Contains options for the gender-tag of the User
 * @author Michelle Rausch
 *
 */
public enum Gender {
	MALE("male"),
	FEMALE("female"),
	ENBY("non-binary"),
	NONE("none"),
	OTHER("other");
	
	/**
	 * The name of the gender
	 */
	private String genderName;
	
	/**
	 * constructs the gender
	 * @param genderName
	 */
	private Gender(String genderName) {
		this.genderName = genderName;
	}
	
	public String getGenderName() {
		return genderName;
	}

}
