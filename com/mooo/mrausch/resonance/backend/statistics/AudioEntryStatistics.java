/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.statistics;

/**
 * This file describes the statistics of an audioentry 
 * @author Michelle Rausch
 *
 */
public class AudioEntryStatistics {
	
	/*
	 * Attributes
	 */
	/** 
	 * the minimum pitch of the user
	 */
	private double averageMinPitch;
	/**
	 * The average pitch of the user
	 */
	private double averagePitch;
	/**
	 * The maximum pitch of the user
	 */
	private double averageMaxPitch;
	
	/**
	 * The minimum resonance of the user
	 */
	private double averageMinResonance;
	/**
	 * The average resonance of the user
	 */
	private double averageResonance;
	
	/**
	 * The maximum resonance of the user
	 */
	private double averageMaxResonance;

	/*
	 * Getters&Setters
	 */
	/**
	 * @return the averageMinPitch
	 */
	public double getAverageMinPitch() {
		return averageMinPitch;
	}

	/**
	 * @param averageMinPitch the averageMinPitch to set
	 */
	public void setAverageMinPitch(double averageMinPitch) {
		this.averageMinPitch = averageMinPitch;
	}

	/**
	 * @return the averagePitch
	 */
	public double getAveragePitch() {
		return averagePitch;
	}

	/**
	 * @param averagePitch the averagePitch to set
	 */
	public void setAveragePitch(double averagePitch) {
		this.averagePitch = averagePitch;
	}

	/**
	 * @return the averageMaxPitch
	 */
	public double getAverageMaxPitch() {
		return averageMaxPitch;
	}

	/**
	 * @param averageMaxPitch the averageMaxPitch to set
	 */
	public void setAverageMaxPitch(double averageMaxPitch) {
		this.averageMaxPitch = averageMaxPitch;
	}

	/**
	 * @return the averageMinResonance
	 */
	public double getAverageMinResonance() {
		return averageMinResonance;
	}

	/**
	 * @param averageMinResonance the averageMinResonance to set
	 */
	public void setAverageMinResonance(double averageMinResonance) {
		this.averageMinResonance = averageMinResonance;
	}

	/**
	 * @return the averageResonance
	 */
	public double getAverageResonance() {
		return averageResonance;
	}

	/**
	 * @param averageResonance the averageResonance to set
	 */
	public void setAverageResonance(double averageResonance) {
		this.averageResonance = averageResonance;
	}

	/**
	 * @return the averageMaxResonance
	 */
	public double getAverageMaxResonance() {
		return averageMaxResonance;
	}

	/**
	 * @param averageMaxResonance the averageMaxResonance to set
	 */
	public void setAverageMaxResonance(double averageMaxResonance) {
		this.averageMaxResonance = averageMaxResonance;
	}
	
	
	
}
