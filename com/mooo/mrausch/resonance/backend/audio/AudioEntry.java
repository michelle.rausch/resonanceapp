/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.audio;

import java.io.File;

import com.mooo.mrausch.resonance.backend.statistics.AudioEntryStatistics;
import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.users.User;

/**
 * This class handles the recordings for each User
 * 		each object is associated with a specific Text, that has been read into the mic (by textID from Database Table Text)
 * 		It belongs to a specific user (userID from Database Table Users)
 * 		it contains the actual recording (wav)
 * 		it contains the statistics for the User
 * 
 * 
 * @author Michelle Rausch
 * 
 * 
 */
public class AudioEntry {

	/*
	 * Attributes
	 */
	/** 
	 * The User who made the recording
	 */
	private User user;
	/** 
	 * The text that has been read in this recording
	 */
	private Text text;

	/**
	 * The statistics of this audio-recording
	 */
	private AudioEntryStatistics statistics;
	
	/**
	 * The recording of the audio, here uncompressed as a .wav TODO: compress (mp3 or ogg)
	 */
	private File audioRecordingFile;
	
	/*
	 * Constructors
	 */
	/**
	 * Constructor of an AudioEntry of User user using the Text text
	 * initializes audio-file
	 * @param user The user reading the text aloud
	 * @param text The text that's read aloud
	 */
	public AudioEntry(User user, Text text) {
		this.text = text;
		this.user = user;
		this.audioRecordingFile = new File("res/audioentries/"+text.getTextID() + "-" + user.getUserID() + ".wav");
	}

	/*
	 * Getters & Setters
	 */
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the text
	 */
	public Text getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(Text text) {
		this.text = text;
	}

	/**
	 * @return the statistics
	 */
	public AudioEntryStatistics getStatistics() {
		return statistics;
	}

	
	/**
	 * @param statistics the statistics to set
	 */
	public void setStatistics(AudioEntryStatistics statistics) {
		this.statistics = statistics;
	}
	/**
	 * @return the audioRecording
	 */
	public File getAudioRecordingFile() {
		return audioRecordingFile;
	}

	/**
	 * @param audioRecordingFile the audioRecording to set
	 */
	public void setAudioRecordingFile(File audioRecordingFile) {
		this.audioRecordingFile = audioRecordingFile;
	}

	
	/*
	 * Methods
	 */



	@Override
	/**
	 * returns the user and the text for this AudioEntry
	 */
	public String toString() {
		return "AudioEntry [user=" + user + ", text=" + text  + "]";
	}
	
}
