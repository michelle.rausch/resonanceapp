/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.audio;

import java.util.List;

/**
 * This Interface describes the operationsfor an AudioEntry
 * it imnplements the DAO-design-patters
 * @author Michelle Rausch
 *
 */
public interface AudioEntryDAO {
	/**
	 * returns the list of all AudioEntrys
	 * @return the list of all AudioEntrys
	 */
	public List<AudioEntry> getAllAudioEntries();
	/**
	 * add an Audioentry
	 * @param audioEntry
	 */
	public void addAudioEntry(AudioEntry audioEntry);
	/**
	 * deletes an Audioentry
	 * @param audioEntry
	 */
	public void deleteAudioEntry(AudioEntry audioEntry);
	/**
	 * updates an AudioEntry
	 * @param audioEntry
	 */
	public void updateAudioEntry(AudioEntry audioEntry );
	
	
	

}
