/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.audio;

import java.util.ArrayList;
import java.util.List;

import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.users.User;

/**
 * This class implements the DAO
 * @author Michelle Rausch
 *
 */
public class AudioEntryDAOimpl implements AudioEntryDAO {
	/*
	 * Attributes
	 */
	/**
	 * The list of all AudioEntries
	 */
	private List<AudioEntry> allAudioEntries;

	/*
	 * Constructors
	 */
	public AudioEntryDAOimpl() {
		this.allAudioEntries = new ArrayList<AudioEntry>();
	}
	
	/*
	 * Methods by DAO
	 */
	@Override
	public List<AudioEntry> getAllAudioEntries() {
		return allAudioEntries;
	}

	@Override
	public void addAudioEntry(AudioEntry audioEntry) {
		allAudioEntries.add(audioEntry);
	}

	@Override
	public void deleteAudioEntry(AudioEntry audioEntry) {
		allAudioEntries.remove(audioEntry);
	}

	//TODO better solution
	@Override
	public void updateAudioEntry(AudioEntry audioEntry) {
		allAudioEntries.remove(getAudioEntryByIDs(audioEntry.getText(), audioEntry.getUser()));
		allAudioEntries.add(audioEntry);
	}
	
	/**
	 * Returns the Audioentry of the text textID belonging to User with userID
	 * @param textID
	 * @param userID
	 * @return Audioentry
	 */
	public AudioEntry getAudioEntryByIDs(Text text, User user) {
		for (AudioEntry audioEntry : allAudioEntries) {
			if (audioEntry.getText().hasSameID(text) && audioEntry.getUser().hasSameID(user)) {
				return audioEntry;
			}
		}	
		return null;
	}

}
