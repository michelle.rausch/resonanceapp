/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.backend.audio;


import com.mooo.mrausch.resonance.backend.text.TextFactory;
import com.mooo.mrausch.resonance.backend.users.UserFactory;

/**
 * This class creates AudioEntrys
 * @author Michelle Rausch
 *
 */
public class AudioEntryFactory {
	/** the text-factory */
	private TextFactory textFactory = new TextFactory();
	/** the user-factory */
	private UserFactory userFactory = new UserFactory();
	
	/**
	 * creates a random AudioEntry out of a random Text and a random User
	 * @return AudioEntry out of a random Text and a random User
	 */
	public AudioEntry getAudioEntry() {
		
		return new AudioEntry(userFactory.createAndReturnSingleUser(), textFactory.getDefaultText());
	}
	
	
	
	

}
