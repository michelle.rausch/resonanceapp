/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.frontend;

import java.io.IOException;

import com.mooo.mrausch.resonance.middletier.IOService;
import com.mooo.mrausch.resonance.middletier.Settings;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This Class contains the main-method of the Application
 * It starts the Main Window of the application
 * contains the IOServices
 * @author Michelle Rausch
 *
 */
public class Main extends Application{

	/**
	 * initializes the app
	 * 		starts the IOService
	 */
	@Override
	public void init() {
		IOService.getinstance();

		
		//initializes settings
		Settings.load();
		Settings.getInstance();
	}
	/**
	 * defines what happens, when App is started
	 * @throws IOException 
	 */
	@Override
	public void start(Stage primaryStage) throws IOException  {

		Parent root = FXMLLoader.load(getClass().getResource("res/MainWindow.fxml") );
		
	    Scene scene = new Scene(root);
	    
	    primaryStage.setTitle("Resonance App");
	    
	    primaryStage.setScene(scene);

	    primaryStage.show();
		
	}

	/**
	 * Launches the program
	 * @param args
	 */
	public static void main(String[] args) {
		launch();

	}
}
