/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.frontend.controllers;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.mooo.mrausch.resonance.backend.users.UserOnMic;
import com.mooo.mrausch.resonance.backend.users.tags.AGAB;
import com.mooo.mrausch.resonance.backend.users.tags.Gender;
import com.mooo.mrausch.resonance.middletier.IOService;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * handles the Window, which edits the User-profile
 * @author Michelle Rausch
 *
 */
public class UserProfileController implements Initializable{
	
	/**
	 * Choicebox which holds the gender of the UserOnMic
	 */
	@FXML
	private ChoiceBox<Gender> genderChoice;
	
	/**
	 * Choicebox which holds the AGAB of the UserOnMic
	 */
	@FXML
	private ChoiceBox<AGAB> agabChoice;
	
	/**
	 * Textfield which holds and sets the Name of the UserOnMic
	 */
	@FXML
	private TextField userNameInput;
	
	/**
	 * Textfield which holds and sets the duration of voicetraining of the UserOnMic
	 */
	@FXML
	private TextField voiceTrainTimeInput;
	
	/**
	 * saves the User when clicked
	 */
	@FXML
	private Button saveUserFromProfile;
	
	@FXML
	protected void handleSaveUserFromProfile() {
		UserOnMic.getInstance().setAgab(agabChoice.getValue());
		UserOnMic.getInstance().setGender(genderChoice.getValue());
		UserOnMic.getInstance().setName(userNameInput.getText());

		
		try {
			IOService.getinstance().getUserOnMicService().save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * cancels editing the User-profile when clicked
	 */
	@FXML
	private Button cancelUserProfile;
	
	/**
	 * exits the User-profile without saving
	 */
	@FXML
	protected void handleCancelUserProfile() {
		((Stage)cancelUserProfile.getScene().getWindow()).close();
	}


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//first initialize/load UserOnMic
		IOService.getinstance().getUserOnMicService().load();

		//fill Choice-boxes with choices
		for (int i = 0; i < Gender.values().length; i++) {
			genderChoice.getItems().add(Gender.values()[i]);
		}
		genderChoice.setValue(UserOnMic.getInstance().getGender());
		
		for (int i = 0; i < AGAB.values().length; i++) {
			agabChoice.getItems().add(AGAB.values()[i]);
		}
		agabChoice.setValue(UserOnMic.getInstance().getAgab());
		
		//fill text fields with data
		userNameInput.setText(UserOnMic.getInstance().getName());
//TODO: when UserStatistics added, add voiceTrainTime to text-field
//		voiceTrainTimeInput.setText(voiceTrainTime);
		
	}




}
