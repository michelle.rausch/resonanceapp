/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.frontend.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import com.mooo.mrausch.resonance.backend.audio.AudioEntry;
import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.users.User;
import com.mooo.mrausch.resonance.backend.users.UserOnMic;
import com.mooo.mrausch.resonance.middletier.IOService;
import com.mooo.mrausch.resonance.middletier.Settings;
import com.mooo.mrausch.resonance.middletier.audio.Recording;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * This controller handles the Controlpanel
 * It contails choices for Mode, Text and Reference as well as the controls for the audio/recording
 * @author Michelle Rausch
 *
 */
public class ControlPanelController implements MainControllerInjectable, Initializable{
	
	/*
	 * helper attributes
	 */
	/**
	 * the Text currently handled
	 */
	private Text activeText;
	/**
	 * The user currently used for reference
	 */
	private User activeReference;
	
	/*
	 * nodes by FXML
	 */
	/**
	 * The MainController this controller belongs to
	 */
	@FXML
	private MainController mainController;
	
	/**
	 * The choice of the text that is to be read/displayed
	 */
	@FXML 
	private ChoiceBox<String> textChoice;
	
	
	/**
	 * The choice of the User, that wil be compared with
	 */
	@FXML
	private ChoiceBox<String> referenceChoice;
	
	/**
	 * shows the progress on the current task (playing back audio / recording, ...)
	 */
	@FXML 
	private ProgressBar audioProgress;
	
	/**
	 * The play/stop-button
	 */
	@FXML 
	private Button playControl;
	
	/**
	 * executes the current mode
	 * regularly plays/stops recording
	 */
	@FXML
	protected void handlePlayAction() { //starts recording/playback
		if (playControl.getText().equals("Play")) {
			playControl.setText("Stop");
			
			//plays back the audio of the reference
			if (Settings.getInstance().isPlaybackActive()) {
				
				System.out.println("play back for User: " + activeReference.getName() + ", Text: " + activeText.getTextName());
				AudioEntry playbackEntry = IOService.getinstance().getAudioEntryService().getSpecificEntry(activeReference, activeText);
				System.out.println(playbackEntry);
				if (playbackEntry!= null) {
					IOService.getinstance().getAudioEntryService().playAudioEntry(playbackEntry);
				}
			}
			
			//records the audio belonging to text
			if (Settings.getInstance().isRecordingActive()) {
				IOService.getinstance().getAudioEntryService().createAudioEntry(activeText);
				AudioEntry recordingEntry = IOService.getinstance().getAudioEntryService().getSpecificEntry(UserOnMic.getInstance(), activeText);
				
				System.out.println("The recording to be save to filesystem");
				System.out.println(recordingEntry);
				System.out.println("UserID: " + Long.valueOf(UserOnMic.getInstance().getUserID()) + "textID: " + activeText.getTextID());
				
				
				IOService.getinstance().getAudioEntryService().recordForAudioEntry(recordingEntry);		
			}
			
			
			
		
		} else { //stops the rocording/playback
			if (!Recording.getInstance().isRecordingOver()) {
				Recording.getInstance().endRecording();
			}
			playControl.setText("Play");
					
		}
	}	
	/**
	 * The label displaying the text which will be read aloud
	 */
	@FXML
	private Label textToRead;

	/**
	 * clicking this button replays the last 10 seconds
	 */
	@FXML
	private Button replayControl;
	
	/**
	 * replays the last 10 seconds of recording (for freetalk-mode)
	 */
	@FXML
	protected void handleReplayAction() {
		System.out.println("pressed replay");
		//TODO
	}
	
	/**
	 * contains, whether recording is active
	 */
	@FXML
	private CheckBox playbackTick;
	
	/**
	 * sets whether recording is active
	 */
	@FXML
	protected void handlePlaybackTick() {
		Settings.getInstance().setRecordingActive(playbackTick.isSelected());
	}
	
	/**
	 * contains, whether playback is active
	 */
	@FXML
	private CheckBox recordTick;
	
	/**
	 * sets whether recording is active
	 */
	@FXML
	protected void handleRecordTick() {
		Settings.getInstance().setRecordingActive(recordTick.isSelected());
	}
	
	/**
	 * contains, whether playback is active
	 */
	@FXML
	private CheckBox loopbackTick;
	
	/**
	 * sets whether recording is active
	 */
	@FXML
	protected void handleLoopbackTick() {
		Settings.getInstance().setRecordingActive(loopbackTick.isSelected());
	}

	@Override
	public void injectMainController(MainController mainController) {
		this.mainController = mainController;
		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		//populate choiceboxes
		IOService.getinstance().getTextService().getAllLocalTexts().forEach(localText -> {
			textChoice.getItems().add(localText.getTextName());
		});

		IOService.getinstance().getUserService().getAllLocalUsers().forEach(localUser -> {
			referenceChoice.getItems().add(localUser.getName());
		});
		
		//sets defaults for choiceboxes
		activeText = IOService.getinstance().getTextService().getAllLocalTexts().get(0);
		textChoice.setValue(activeText.getTextName());
		activeReference = IOService.getinstance().getUserService().getAllLocalUsers().get(0);
		referenceChoice.setValue(activeReference.getName());
		
		//sets the active Text on change	
		textChoice.setOnAction(event  ->{
			int selectedTextChoice = textChoice.getItems().indexOf(textChoice.getValue());
			activeText = IOService.getinstance().getTextService().getAllLocalTexts().get(selectedTextChoice);
			textToRead.setText(activeText.getTextToSay());		
		});
		
		//sets active Reference-User on change
		referenceChoice.setOnAction(event -> {
			int selectedUserChoice = referenceChoice.getValue().indexOf(referenceChoice.getValue());
			activeReference = IOService.getinstance().getUserService().getAllLocalUsers().get(selectedUserChoice);		
		});
		
		//sets check-boxes
		recordTick.setSelected(Settings.getInstance().isRecordingActive());
		playbackTick.setSelected(Settings.getInstance().isPlaybackActive());
		loopbackTick.setSelected(Settings.getInstance().isLoopbackActive());
		recordTick.setOnAction(tick ->{
			Settings.getInstance().setRecordingActive(recordTick.isSelected());
		});
		loopbackTick.setOnAction(tick ->{
			Settings.getInstance().setLoopbackActive(loopbackTick.isSelected());
		});
		playbackTick.setOnAction(tick ->{
			Settings.getInstance().setPlaybackActive(playbackTick.isSelected());
		});
		
		
		
		
		
	}
	
}
