/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.frontend.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

/**
 * @author Michelle Rausch
 *
 */
public class HelpMenuController implements MainControllerInjectable {
	
	/**
	 * The MainController this controller belongs to
	 */
	@FXML
	private MainController mainController;
	
	/**
	 * Clicking on this MenuItem opens the Manual
	 */
	@FXML
	private MenuItem manualItem;
	
	/**
	 * opens the Manual
	 */
	@FXML
	protected void handleManualMenuAction() {
		//TODO
	};
	
	/**
	 * Clicking on this MenuItem opens the "About: Resonance" Window
	 */
	@FXML
	private MenuItem aboutItem;
	
	/**
	 * opens the "About: Resonance" Window
	 */
	@FXML
	protected void handleAboutMenuAction() {
		//TODO
	}
	
	

	@Override
    public void injectMainController(MainController mainController) {
    	this.mainController = mainController;
    }

}
