/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.frontend.controllers;



import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
/**
 * The controller for the User-menu
 * 	contains:
 * 		Item: User-Profile
 * 		Item: Save User
 * 		Item: Set Preferences
 * 		Item: load REference
 * 		Item: Quit-button 
 * @author Michelle Rausch
 *
 */
public class UserMenuController implements MainControllerInjectable{
	
	/**
	 * The MainController this controller belongs to
	 */
	@FXML
	private MainController mainController;
	
	
	/**
	 * The MenuItem for Setting the User-profile
	 */
	@FXML
	private MenuItem userProfileItem;
	
	/**
	 * Opens a User-profile-Editor on click
	 * @param event
	 * @throws IOException 
	 */
	@FXML
	protected void handleUserMenuAction(ActionEvent event) throws IOException {
		Stage userProfileWindow = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("res/UserProfile.fxml"));	
		userProfileWindow.setScene(new Scene(root));
		
		userProfileWindow.show();
		
		
	}
	
	/**
	 * The MenuItem for setting the Preferences
	 */
	@FXML
	private MenuItem preferenceItem;
	
	/**
	 * Sets the Preferences on click
	 * @param event
	 */
	@FXML
	protected void handlePreferenceMenuAction(ActionEvent event) {
		//TODO
	}
	
	
	/**
	 * The MenuItem for saving the current User and their AudioEntrys
	 */
	@FXML
	private MenuItem saveUser;
	
	/**
	 * Saves User on click
	 * @param event
	 */
	@FXML
	protected void handleSaveUserAction(ActionEvent event) {
	}
	
	/**
	 * The Button to load a reference-User
	 */
    @FXML 
    private MenuItem loadReference;
    
    /**
     * Opens Selection-screen of to load a user into references
     * @param event click on MenuItem
     */
    @FXML 
    protected void handleLoadReferenceAction(ActionEvent event) {
    	//TODO
    }
	
	
	/**
	 * The Quit-MenuItem in the User-menu
	 */
    @FXML 
    private MenuItem quitItem;
    
    /**
     * Exits the program
     * @param event click on MenuItem
     */
    @FXML 
    protected void handleQuitMenuAction(ActionEvent event) {
    	System.exit(-1);
    }
	
    
    @Override
    public void injectMainController(MainController mainController) {
    	this.mainController = mainController;
    }

}
