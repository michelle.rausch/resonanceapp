/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package com.mooo.mrausch.resonance.frontend.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * The controller of the main window
 * Functions as the parent-controller of elements of the main-window
 * @author Michelle Rausch
 *
 */
public class MainController implements Initializable{
	/**
	 * The controller for the User-Menu
	 */
	@FXML
	private UserMenuController userMenuController;
	
	/**
	 * The controller for the Help-Menu
	 */
	@FXML
	private HelpMenuController helpMenuController;
	
	/**
	 * The controller for the visualization of audio and statistics
	 */
	@FXML
	private VisualizationTabsController visualizationTabsController;
	
	/**
	 * The controller for the control-panel
	 */
	@FXML 
	private ControlPanelController controlPanelController;

	/**
	 * Intilializes the Main-Window
	 * 		Injects this (the main-Controller) into the child-controllers
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		userMenuController.injectMainController(this);
		helpMenuController.injectMainController(this);
		visualizationTabsController.injectMainController(this);
		controlPanelController.injectMainController(this);
		
	}
}
