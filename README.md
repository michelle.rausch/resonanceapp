# ResonanceApp

```
 ____                                            
|  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
| |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
|  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
|_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
```

The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 
Resonance helps with voice-training, especially for transgender women.
What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.

It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP

Resonance is licensed under GNU General Public License v3.0.

