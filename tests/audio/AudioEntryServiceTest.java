/**
 * 
 */
package tests.audio;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.mooo.mrausch.resonance.backend.audio.AudioEntry;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryDAO;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryDAOimpl;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryFactory;
import com.mooo.mrausch.resonance.backend.text.DefaultText;
import com.mooo.mrausch.resonance.backend.users.User;
import com.mooo.mrausch.resonance.backend.users.UserOnMic;
import com.mooo.mrausch.resonance.middletier.audio.AudioEntryService;

/**
 * Tests more functionality of the Service, especially recording and file-writing
 * @author Michelle Rausch
 *
 */
public class AudioEntryServiceTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner waiter = new Scanner(System.in);
		
		AudioEntryService service;
		
		AudioEntryDAO source;
		
		AudioEntryFactory factory = new AudioEntryFactory();
		
		source = new AudioEntryDAOimpl();
		
		System.out.println("creates a random AudioEntry:");
		source.addAudioEntry(new AudioEntry(UserOnMic.getInstance(), DefaultText.FIRE.getTextObj()));
		source.addAudioEntry(factory.getAudioEntry());
		source.addAudioEntry(factory.getAudioEntry());
		source.addAudioEntry(factory.getAudioEntry());
		source.addAudioEntry(factory.getAudioEntry());
		source.addAudioEntry(factory.getAudioEntry());


		source.getAllAudioEntries().forEach(entry ->  {
			System.out.println(entry);
		});
		
		service = new AudioEntryService(source);
		
		System.out.println("Prints all Entries with Text FIRE");
		
		service.getAllAudioEntrysWithText(DefaultText.FIRE.getTextObj()).forEach( entry ->{
			System.out.println(entry);
		});
		
		System.out.println("Test getting a specific entry");
		AudioEntry specificEntry;
		specificEntry = service.getSpecificEntry((User)UserOnMic.getInstance(), DefaultText.FIRE.getTextObj());
		System.out.println(specificEntry);
		
		
		System.out.println("record for AudioEntry 0");
//		service.recordForAudioEntry(source.getAllAudioEntries().get(0));
		
		
		File audioFile = source.getAllAudioEntries().get(0).getAudioRecordingFile();
//		System.out.println("name of file: " + audioFile.toString());

		System.out.println("play back recording");
		
		System.out.println("press any button to play back audio");
		waiter.next();
//		playback.start();
		
//		service.playAudioEntry(source.getAllAudioEntries().get(0));
	
		System.out.println("press any button to end program");
		waiter.next();
	}
	
}
