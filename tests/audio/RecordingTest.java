/**
 * 
 */
package tests.audio;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;

import com.mooo.mrausch.resonance.middletier.audio.Recording;

/**
 * This class tests the recording of audio
 *  
 *  
 *  It records and saves as .wav
 * @author Michelle Rausch
 *
 */
public class RecordingTest {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		File recordingFile = new File("testing/testRecording.wav");
		
		Recording recording = Recording.getInstance();
		
		int recordTime = 3_000;
		recording.recordForTime(recordTime);
		
		AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;
		AudioSystem.write(recording.getInputStream(), fileType, recordingFile);
		
		
		try {
			Thread.sleep(recordTime);
		} catch (InterruptedException e) {
		}
		

		
	}

}
