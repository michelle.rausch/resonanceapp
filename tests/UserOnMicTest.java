/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package tests;

import java.io.IOException;

import com.mooo.mrausch.resonance.backend.users.UserOnMic;
import com.mooo.mrausch.resonance.backend.users.tags.AGAB;
import com.mooo.mrausch.resonance.backend.users.tags.Gender;
import com.mooo.mrausch.resonance.middletier.users.UserOnMicService;

/**
 * @author Michelle Rausch
 *
 */
public class UserOnMicTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		UserOnMic.getInstance();

		UserOnMicService service = new UserOnMicService();

		System.out.println(service.getUserOnMicFile());

		UserOnMic.getInstance().setAgab(AGAB.FEMALE);

		UserOnMic.getInstance().setGender(Gender.ENBY);

		UserOnMic.getInstance().setName("Pax");
		
		System.out.println(UserOnMic.getInstance());

		try {
			service.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		

		service.load();

		
		System.out.println(UserOnMic.getInstance());
		
		
		

	}

}
