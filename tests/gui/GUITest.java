/**
 * 
 */
package tests.gui;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Starts the Application and opens the Main Menu
 * @author Michelle Rausch
 *
 */
public class GUITest extends Application{
	

    
	@Override
	public void start(Stage primaryStage) throws IOException  {

				
		Parent root = FXMLLoader.load(getClass().getResource("res/MainMenu.fxml"));
		
        Scene scene = new Scene(root);

        
        primaryStage.setTitle("GUI-Test");
        
        primaryStage.setScene(scene);

        primaryStage.show();	

	}
	
	
	
	public static void main(String[] args) {
		launch();
	}
	
}
