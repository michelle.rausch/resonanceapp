/**
 *  ____                                            
 * |  _ \ ___  ___  ___  _ __   __ _ _ __   ___ ___ 
 * | |_) / _ \/ __|/ _ \| '_ \ / _` | '_ \ / __/ _ \
 * |  _ <  __/\__ \ (_) | | | | (_| | | | | (_|  __/
 * |_| \_\___||___/\___/|_| |_|\__,_|_| |_|\___\___|
 * 
 * The Resonance App is developed by Michelle Rausch: https://gitlab.com/michelle.rausch
 * 
 * Resonance helps with voice-training, especially for transgender women.
 * What sets this app aside is the ability to display the resonance of the voice in addition to the pitch.
 * 
 * It is based on TarsosDSG by JorenSix: https://github.com/JorenSix/TarsosDSP
 * 
 * Resonance is licensed under GNU General Public License v3.0.
 */
package tests.analyze;

import javax.sound.sampled.LineUnavailableException;

import com.mooo.mrausch.resonance.middletier.audio.Recording;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.jvm.JVMAudioInputStream;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;
import be.tarsos.dsp.pitch.PitchProcessor.PitchEstimationAlgorithm;

/**
 * This class displays the pitch of an audioInput stream realtime
 * @author Michelle Rausch
 *
 */
public class CurrentPitchFinder {

	/**
	 * @param args
	 * @throws LineUnavailableException 
	 */
	public static void main(String[] args) throws LineUnavailableException {
		Recording recording  = Recording.getInstance();
		
		int bufferSize = 1024 * 4; // size of buffer
		int overlap = 100;
		float sampleRate = recording.getFormat().getSampleRate();
		
		recording.startRecording(); //starts recording, ned to be started before stream is set, otherwise steam is null
		JVMAudioInputStream stream = null;
		AudioDispatcher dispatcher = null;
		
		if (recording.getInputStream() != null) {
			stream = new JVMAudioInputStream(recording.getInputStream());	
			dispatcher = new AudioDispatcher(stream, bufferSize, overlap);
		}

		
		PitchDetectionHandler handler = new PitchDetectionHandler() {
			
			@Override
			public void handlePitch(PitchDetectionResult pitchDetectionResult, AudioEvent audioEvent) {
				System.out.println(pitchDetectionResult.getPitch());
				
			}
		};
		
		AudioProcessor pitchProcessor = new PitchProcessor(PitchEstimationAlgorithm.AMDF, sampleRate, bufferSize, handler);
		
		recording.startRecording();
		
		Thread waiter = new Thread(() -> {
			try {
				Thread.sleep(5_000);
			} catch (InterruptedException e) {
			}
			recording.endRecording();
		});
		
		if (dispatcher != null) {
			dispatcher.addAudioProcessor(pitchProcessor);
			dispatcher.run();
		}
		
		dispatcher.stop();
		recording.endRecording();
	}

}
