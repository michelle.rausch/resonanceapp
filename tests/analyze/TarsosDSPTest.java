package tests.analyze;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.mooo.mrausch.resonance.middletier.audio.Recording;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.jvm.AudioDispatcherFactory;
import be.tarsos.dsp.util.fft.FFT;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * This class will help with getting to know TarsosDSP as well as JavaFX
 * 
 * @author Michelle Rausch
 *
 */
public class TarsosDSPTest {

	public static void main(String[] args) throws InterruptedException, UnsupportedAudioFileException, IOException {
		/*
		 * First, record a 3s input stream and save
		 */
		File recordingFile = new File("audioentries/0-0.wav");

//		Recording recording = new Recording();
//		
//		int recordTime = 3_000;
//		recording.recordForTime(recordTime);
//		
//		AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;
//		AudioSystem.write(recording.getInputStream(), fileType, recordingFile);		
//
//		Thread.sleep(recordTime);

		/*
		 * Then do a FFT
		 */
		int bufferSize = 1024 * 4; // size of buffer
		int overlap = 100;

//		AudioFormat recordingFormat = recording.getInputStream().getFormat();
//		TarsosDSPAudioFormat format = new TarsosDSPAudioFormat(
//				recordingFormat.getSampleRate(),
//				recordingFormat.getSampleSizeInBits(), 
//				recordingFormat.getChannels(),
//				true,
//				true);
//		JVMAudioInputStream stream = new JVMAudioInputStream(recording.getInputStream());	

//		AudioDispatcher dispatcher = new AudioDispatcher(stream, bufferSize, overlap);	//shops up audio stream into blocks with size bufferSize and an overlap
		AudioDispatcher dispatcher = AudioDispatcherFactory.fromFile(recordingFile, bufferSize, overlap);

		AudioProcessor fftProcessor = new AudioProcessor() {

			FFT fft = new FFT(bufferSize); // defines the FFT
			float[] amplitudes = new float[bufferSize / 2]; // the amplitudes for each bin

			@Override
			public void processingFinished() {
				System.out.println("processing finished");
			}

			@Override
			public boolean process(AudioEvent audioEvent) {
				float[] audioFloatBuffer = audioEvent.getFloatBuffer();
				float[] transformbuffer = new float[bufferSize * 2];

				System.arraycopy(audioFloatBuffer, 0, transformbuffer, 0, audioFloatBuffer.length);

				fft.forwardTransform(transformbuffer);
				fft.modulus(transformbuffer, amplitudes);

				// Output syst sysout
				for (int i = 0; i < amplitudes.length; i++) {
					System.out.println(amplitudes[i]);
				}

				return true;
			}
		};

		System.out.println("starting FFT-process");

		dispatcher.addAudioProcessor(fftProcessor);

		dispatcher.run();

		System.out.println("FFT over");

	}

}
