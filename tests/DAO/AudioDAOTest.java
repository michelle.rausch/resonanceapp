/**
 * 
 */
package tests.DAO;

import java.util.List;

import com.mooo.mrausch.resonance.backend.audio.AudioEntry;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryDAO;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryDAOimpl;
import com.mooo.mrausch.resonance.backend.audio.AudioEntryFactory;
import com.mooo.mrausch.resonance.middletier.audio.AudioEntryService;

/**
 * Tests basic DAO-functions by the service.
 * 		recordings are tested in ../audio/AudioEntryServiceTest.java
 * @author Michelle Rausch
 *
 */
public class AudioDAOTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		AudioEntryDAO audioSource = new AudioEntryDAOimpl();
		
		List<AudioEntry> allAudioEntrys;
		
		AudioEntryService service = new AudioEntryService(audioSource);
		
		AudioEntryFactory factory = new AudioEntryFactory();
		
		service.getAllAudioEntrys().add(factory.getAudioEntry());		
		service.getAllAudioEntrys().add(factory.getAudioEntry());
		service.getAllAudioEntrys().add(factory.getAudioEntry());
		service.getAllAudioEntrys().add(factory.getAudioEntry());
		service.getAllAudioEntrys().add(factory.getAudioEntry());
		
		service.getAllAudioEntrys().forEach(entry -> {
			System.out.println(entry);
		});
		
	}

}
