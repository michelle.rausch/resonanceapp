/**
 * 
 */
package tests.DAO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.mooo.mrausch.resonance.backend.text.Text;
import com.mooo.mrausch.resonance.backend.text.TextDAO;
import com.mooo.mrausch.resonance.backend.text.TextDAOimpl;
import com.mooo.mrausch.resonance.backend.text.TextFactory;
import com.mooo.mrausch.resonance.middletier.text.TextService;

/**
 * Tests the TextDAO including factory
 * @author Michelle Rausch
 *
 */
public class TextDAOTest {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException {
		
		TextDAO textSource;
		
		List<Text> allTexts;
		
		textSource = new TextDAOimpl();
		
		allTexts = textSource.getAllTexts();
		
		System.out.println("add some sample texts");
		
		
		TextService service = new TextService(textSource);
		
		TextFactory factory = new TextFactory();
		
		
//		service.getAllTexts().add(factory.getDefaultText());
//		service.getAllTexts().add(factory.getDefaultText());
//		service.getAllTexts().add(factory.getDefaultText());
//		service.getAllTexts().add(factory.getDefaultText());
//		service.getAllTexts().add(factory.getDefaultText());
//		service.addText(new Text("Go vegan!", "veg", Locale.ENGLISH));
//		service.addText(new Text("2021 wil be the year of Linux on the Desktop", "YTLD", Locale.ENGLISH));
		
//		Text textToAdd = new Text("2021 wil be the year of Linux on the Desktop", "YTLD", Locale.ENGLISH);
		
		System.out.println("created texts:");
		service.getAllTexts().forEach(text -> {
			System.out.println(text);
		});
		

//		try {
//			service.saveTextLocally(allTexts.get(0));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		
//		service.getAllTexts().forEach(text -> {
//			try {
//				service.saveTextLocally(text);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		});
		
		System.out.println("load texts from files");
		service.loadAllLocalTexts();
		
		service.getAllLocalTexts().forEach(text -> {
			System.out.println(text);
		});
	
		service.getAllLocalTexts().get(1).setRecordingTime(3000);
		
		service.saveTextLocally(service.getAllLocalTexts().get(1));

	}

}
