/**
 * 
 */
package tests.DAO;


import com.mooo.mrausch.resonance.backend.users.UserDAOimpl;
import com.mooo.mrausch.resonance.backend.users.UserFactory;
import com.mooo.mrausch.resonance.backend.users.UserOnMic;
import com.mooo.mrausch.resonance.backend.users.UsersDAO;
import com.mooo.mrausch.resonance.middletier.users.UserService;

/**
 * Tests the UserDAO
 * @author Michelle Rausch
 *
 */
public class UserDAOTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		UserOnMic.getInstance();
		
		
		UsersDAO source = new UserDAOimpl();		
	
		
		UserService service = new UserService(source);
		
		source = new UserDAOimpl();
		
		UserFactory factory = new UserFactory();
		
		for (int i = 0; i < 10; i++) {
			service.addUser(factory.createAndReturnSingleUser());
		}
		
		service.getAllUsers().forEach(user -> {
			System.out.println(user); 
		});
		
		System.out.println("ID for entry 5: " + service.getAllUsers().get(5).getUserID());
		
		
	
		service.loadAllLocalUsersSeperate();

		
		service.getAllLocalUsers().forEach(user -> {
			System.out.println(user);
		});		
		
		
//		try {
//			service.saveUserLocally(service.getAllUsers().get(1));
//		} catch (IOException e) {
//			System.out.println("saving failed");
//		}
		
		
		
	}

}
